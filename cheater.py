#!/usr/bin/env python3
import collections
import re
import sys
from pprint import pprint


def split_answer(answer):
    answer = re.sub(
        r"-", " ", re.sub(r"\)", " )", re.sub(r"\(", "( ", answer.strip().lower()))
    )
    if not len(answer):
        return []
    yield answer
    if " " in answer:
        yield from answer.split(" ")
    number = re.match(r"^(\d+)\s", answer)
    num = None
    if number is not None:
        num = number.groups()[0]
        yield f"{num}>"
    else:
        number = re.match(r"^(\d+(\.\d+)?)$", answer)
        if number is not None:
            num = number.groups()[0]
    if num is not None:
        if num.endswith("000"):
            yield f'{re.sub(r"(000)+$", "", num)}...'
        elif num.endswith("0"):
            yield f'^{re.sub(r"0+$", "", num)}'
        elif "." in num:
            yield re.sub(r"\..*", "", num) + "."
            yield "." + re.sub(r".*\.", "", num)
    else:
        yield f"^{answer[0]}"


def process_answer(answer, fname):
    yield from split_answer(answer)
    yield from (f"{fname}::{answ}" for answ in split_answer(answer))


def get_questions():
    for fname in sys.argv[1:]:
        if fname.startswith("-"):
            continue
        with open(fname, "r") as f:
            while True:
                q = f.readline()
                p = f.readline()
                n = f.readline()
                if not n:
                    break
                yield (
                    q.strip().lower(),
                    {",".join(process_answer(a, fname)) for a in p.split(",")},
                    {",".join(process_answer(a, fname)) for a in n.split(",")},
                )


allpos = set()
allneg = set()
positives = collections.defaultdict(lambda: set())
negatives = collections.defaultdict(lambda: set())
to_map = []

for text, pos, neg in get_questions():
    text = re.sub(r"[,.]( |$)", " ", re.sub(r"[^\x20-\x7f]", "", text))
    for answ in pos:
        if len(answ):
            to_map.append((answ.split(","), text))
    pos = {wrd for answ in pos for wrd in answ.split(",")}
    neg = {wrd for answ in neg for wrd in answ.split(",")}
    for wrd in text.split(" "):
        if not wrd or len(wrd) < 4:
            continue
        allpos |= pos
        allneg |= neg
        positives[wrd] |= pos
        negatives[wrd] |= neg

allpos -= allneg
toRm = set()
for v in allpos:
    if v.endswith(">"):
        toRm |= {
            x for x in allpos if re.match(rf"^{v[:-1]}0*(\.\.\.)?$", x) is not None
        }
allpos -= toRm

for k, v in negatives.items():
    positives[k] -= v


def cleanval(v):
    return re.sub(r",[^,]*>|,[^,]*\.\.\.|,\^[^,]*", "", v)


result = collections.defaultdict(lambda: set())
if len(allpos):
    kw = ""
    vals = allpos
    ml = len(to_map)
    for v in sorted(vals, key=lambda x: len(x)):
        to_map = [m for m in to_map if v not in m[0] or kw not in m[1]]
        l2 = len(to_map)
        if l2 == ml:
            continue
        ml = l2
        result[kw] |= {cleanval(v)}
        if not ml:
            break

# ranked = sorted(positives.items(), key=lambda x: len(x[1]), reverse=True)
ranked = sorted(
    (
        (k, [v], len(it))
        for k, it in positives.items()
        for v in it
        if not any(vs in toRm for vs in v)
    ),
    key=lambda x: x[2] + len([x for x in to_map if v in x[0] and k in x[1]]) * 10,
    reverse=True,
)


while len(to_map):
    if not len(ranked):
        break
    (kw, vals, _), *ranked = ranked
    ml = len(to_map)
    for v in sorted(vals, key=lambda x: len(x)):
        to_map = [m for m in to_map if v not in m[0] or kw not in m[1]]
        l2 = len(to_map)
        if l2 == ml:
            continue
        ml = l2
        result[kw] |= {cleanval(v)}
        if not ml:
            break

if len(to_map):
    print("Could not find solution for", [f"{x[1]}: {x[0][0]}" for x in to_map])

pprint(result)

"""
12... - multiple of 1k times 12
15> - number starting with 15 and some text after
^xy - answer starting with xy
"""
